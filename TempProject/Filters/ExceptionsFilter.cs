﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TempProject.Filters
{
    public class ExceptionsFilter : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;

            filterContext.ExceptionHandled = true;
            filterContext.HttpContext.Response.Clear();

            ViewDataDictionary viewData = new ViewDataDictionary();
            viewData.Add("ErrorMessage", ex.Message);
            filterContext.Result = new ViewResult()
            {
                ViewName = "Index",
                ViewData = viewData
            };

            base.OnException(filterContext);   
        }
    }
}