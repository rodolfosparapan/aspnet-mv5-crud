﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace TempProject.Models
{
    public class Person
    {
        public int? Id { get; set; }
        public int Code { get; set; }
        public string Name { get; set; }
        public GenderTypes Gender { get; set; }
    }
}