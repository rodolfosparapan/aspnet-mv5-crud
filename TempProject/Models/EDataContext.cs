﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TempProject.Models;

namespace TempProject.Models
{
    public class EDataContext : DbContext
    {
        public EDataContext() : base("SistemaDatabase") { }

        public DbSet<Person> Person { get; set; }
    }
}