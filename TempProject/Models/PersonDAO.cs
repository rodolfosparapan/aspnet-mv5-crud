﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TempProject.Models
{
    public class PersonDAO
    {
        private EDataContext context;
        
        public PersonDAO(EDataContext context)
        {
            this.context = context;
        }

        public List<Person> GetAll()
        {
            return this.context.Person.ToList();
        }

        public void Add(Person person)
        {
            this.context.Person.Add(person);
            this.context.SaveChanges();    
        }

        public void Update(Person person)
        {
            Person entity = this.context.Person.Find(person.Id);
            if (entity == null)
                return;
            
            this.context.Entry(entity).CurrentValues.SetValues(person);
            context.SaveChanges();
        }

        public void Remove(int id)
        {    
            Person person = context.Person.Find(id);
            context.Person.Remove(person);
            context.SaveChanges();   
        }
    }
}