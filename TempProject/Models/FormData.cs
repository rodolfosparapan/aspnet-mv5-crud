﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace TempProject.Models
{
    public class FormData
    {
        public int? Id { get; set; }

        [Required(ErrorMessage = "A Code is required")]
        public int Code { get; set; }

        [Required(ErrorMessage = "A Name is required")]
        public string Name { get; set; }

        [EnumDataType(typeof(GenderTypes), ErrorMessage = "Invalid Gender")]
        public GenderTypes Gender { get; set; }

        [EnumDataType(typeof(PersonTypes), ErrorMessage = "Invalid Person")]
        public PersonTypes PersonType { get; set; }

        public string ToJSON()
        {
            return new JavaScriptSerializer().Serialize(this);
        }
    }

    public enum GenderTypes { Male, Female };
    public enum PersonTypes { Customer, Employee };
}