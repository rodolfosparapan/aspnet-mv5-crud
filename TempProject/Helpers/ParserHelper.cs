﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TempProject.Models;

namespace TempProject.Helpers
{
    public class ParserHelper
    {
        public static Person GetPerson(FormData formData)
        {
            Person person = new Customer();
            switch (formData.PersonType)
            {
                case PersonTypes.Customer:
                    person = new Customer() { Id = formData.Id, Code = formData.Code, Name = formData.Name, Gender = formData.Gender };
                    break;

                case PersonTypes.Employee:
                    person = new Employee() { Id = formData.Id, Code = formData.Code, Name = formData.Name, Gender = formData.Gender };
                    break;
            }
            return person;
        }

        public static List<FormData> GetFormDataList(List<Person> people)
        {
            List<FormData> formData = new List<FormData>();
            foreach (Person person in people)
            {
                formData.Add(new FormData() { Id=person.Id, Code=person.Code, Name=person.Name,
                                              Gender = person.Gender,
                                              PersonType = ParserHelper.GetPersonEnum(person.GetType())
                });
            }
            return formData;
        }

        private static PersonTypes GetPersonEnum(Type type)
        {
            PersonTypes person = PersonTypes.Customer;
            Enum.TryParse(type.ToString().Split('.').ToArray().Last(), out person);
            return person;
        }
    }
}