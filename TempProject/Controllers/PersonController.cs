﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TempProject.Helpers;
using TempProject.Models;

namespace TempProject.Controllers
{
    public class PersonController : Controller
    {
        private PersonDAO personDAO;
        public PersonController(PersonDAO personDAO)
        {
            this.personDAO = personDAO;
        }

        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.People = ParserHelper.GetFormDataList(personDAO.GetAll());
            return View("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormData formData)
        {
            if (ModelState.IsValid)
            {
                this.personDAO.Add(ParserHelper.GetPerson(formData));
                ViewBag.SuccessMessage = "Person added successfully!";
                ModelState.Clear();
            }

            return this.Index();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(FormData formData)
        {
            if (ModelState.IsValid)
            {
                this.personDAO.Update(ParserHelper.GetPerson(formData));
                ViewBag.SuccessMessage = "Person updated successfully!";
            }

            return this.Index();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(FormData formData)
        {
            if (ModelState.IsValid)
            {
                this.personDAO.Remove((int)ParserHelper.GetPerson(formData).Id);
                ViewBag.SuccessMessage = "Person removed successfully!";
                ModelState.Clear();
            }

            return this.Index();
        }
	}
}